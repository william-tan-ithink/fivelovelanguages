import React from 'react'
import banner from './img/banner.jpg'
import 'bootstrap/dist/css/bootstrap.css'
import './App.css'
import Footer from './components/Footer'
import TestPage from "./components/TestPage"
import Result from "./components/Result"
import questionData from "./data/single"

const defaultAnswers = questionData.reduce((result, item) => {
  result[item.name] = null
  return result
}, {})

const answers = defaultAnswers
// const answers = {
//   q1: "E", q2: "D", q3: "B", q4: "D", q5: "E", q6: "B", q7: "C", q8: "E", q9: "B", q10: "A",
//   q11: "B", q12: "D", q13: "A", q14: "B", q15: "A", q16: "B", q17: "D", q18: "A", q19: "D", q20: "D",
//   q21: "B", q22: "C", q23: "C", q24: "B", q25: "D", q26: "E", q27: "A", q28: "C", q29: "D", q30: "E",
// }

// const answers = {
//   q1: "E", q2: "D", q3: "B", q4: "D", q5: "E", q6: "B", q7: "C", q8: "E", q9: "B", q10: "A",
//   q11: "B", q12: "D", q13: "A", q14: "B", q15: "A", q16: "B", q17: "D", q18: "A", q19: "D", q20: "D",
//   q21: "B", q22: "C", q23: "C", q24: "B", q25: "D", q26: "E", q27: "A", q28: "C", q29: "D", q30: null,
// }


export const AnswerContext = React.createContext({
  answers
})

function App() {
  const setAnswers = (name, ans) => {
    updateAnswers(prevState => {
      const { answers } = prevState
      const newAnswers = { ...answers, [name]: ans }
      return { ...prevState, answers: newAnswers }
    })
  }

  const answerState = {
    answers,
    setAnswers
  }

  const [state, updateAnswers] = React.useState(answerState)

  const [showResults, updateShowResults] = React.useState(false)

  function handleOnSubmitClicked() {
    updateShowResults(true)
    window.scrollTo(0,0)
  }

  function onResetClicked() {
    updateShowResults(false)
    updateAnswers(p => {
      return { ...p, answers: defaultAnswers }
    })
    window.scrollTo(0,0)
  }

  return (
    <AnswerContext.Provider value={state}>
      <main>
        <div className="text-center">
          <img className="w-100" src={banner}/>
        </div>
        <div className="container offset-container mb-5">
          {showResults ? <Result answers={state.answers} onResetClicked={onResetClicked}/> :
            <TestPage handleOnSubmitClicked={handleOnSubmitClicked}/>}
        </div>
        <Footer/>
      </main>
    </AnswerContext.Provider>
  )
}

export default App
