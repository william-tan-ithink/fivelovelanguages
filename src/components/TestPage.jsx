import React from 'react'
import Question from './Question'
import questionData from '../data/single'
import { AnswerContext } from '../App'

export default function TestPage({ handleOnSubmitClicked }) {

  const { answers } = React.useContext(AnswerContext)

  const unAnswered = Object.entries(answers).reduce((results, [k, v]) => {
    if (v === null)
      results.push(k)
    return results

  }, [])

  function renderQuestion(q) {
    return (
      <li id={q.name} key={q.name}>
        <div className="numbering-border-bottom"/>
        <Question id={q.name + "-q1"} name={q.name} answer={q.q1.ans} questionEn={q.q1.en} questionZh={q.q1.zh}
                  checked={answers[q.name] === q.q1.ans}/>
        <Question id={q.name + "-q2"} name={q.name} answer={q.q2.ans} questionEn={q.q2.en} questionZh={q.q2.zh}
                  checked={answers[q.name] === q.q2.ans}/>
      </li>
    )
  }

  function renderUnAnsweredList(item) {
    return (
      [<a key={item} className="font-weight-bold" href={"#" + item}>{item.replace('q', '')}</a>, <span key={item + "-comma"}>, </span>]
    )
  }

  return (
    <section className="mb-5">
      <form className="test">
        <ol className="mb-5">
          {questionData.map(q => renderQuestion(q))}
        </ol>
        <button className="btn btn-submit btn-block btn-lg mb-3"
                disabled={unAnswered.length > 0}
                onClick={handleOnSubmitClicked}>测试分析</button>
      </form>
      {unAnswered.length > 0 && (<div className="alert alert-danger" role="alert">
        <span>你还有第 </span>
        {unAnswered.map(item => renderUnAnsweredList(item)).flat().slice(0, -1)}
        <span> 题还没回答。</span>
      </div>)}
    </section>
  )
}
